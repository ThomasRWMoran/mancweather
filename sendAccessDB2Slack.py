#!/usr/bin/env python3
import json
import sys
import requests

def getPayload(message):
    dPayload = {"channel": "#academy_api_testing", "username": "mancWeather DB bot"}
    if type(message) != str:
        print('Message not string')
        sys.exit(21)
    else:
        dPayload['text'] = message
    jPayload = json.loads(json.dumps(dPayload))
    return jPayload

def send2Slack(message):
    SLACK_URL = ""
    rSlack = requests.post(SLACK_URL, json=getPayload(message))
    if rSlack.status_code != 200:
        print("requests.post error")
        sys.exit(20)

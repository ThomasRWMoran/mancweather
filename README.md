# README #

### tools used  ###
Editor for developing -> Atom (languages Python & Shell, plug-in Git); 
Communication -> Slack;
Project Management & Planning -> Trello;
Source/Version Control -> Bitbucket & Git;
AWS Instance -> database.academy.grads.al-labs.co.uk;
Accessing Database -> mysql-client (CLI program) & MySQLWorkbench
(GUI application);

### runDB.sh ###
Overall program to automate setting up the database, request weather
info, upload weather data into the database, update the data and
check for severe weather temperatures.
Bash script that runs through the whole list of cities and uploads
the weather (each 80 seconds)
[TM]

### createDB file ###
Sets up structure of empty DB using mancweather-schema.sql
[SAH]

### mancweather-schema.sql ###
Database schema defining database name, the tables in the database
with their attributes and constraints (defining value types and
primary/foreign keys)
[SAH]

### writeContinents2DB ###
Loads up continent data into database (name and woeid);
This is done manually, cannot be requested from API
[TM]

### city.py###
Uses city.txt to load in the wanted city info from API to the DB
(needs to check for duplicate city names).
Gets info for each city, load up contry, province and city tables
Uses continent codes and loads up woeid to request data from API
[TM]

### city_list.txt###
List of cities that we want to store info for
[OUTPUT]

### city_dictionary.txt ###
Lists of cities with data loaded up by running <city.py>, data found in
the database as well
[OUTPUT]

### requestWeatherData.py ###
Requests weather info for a specified city woeid and loads up the
weather related tables from the database  
[SAH]

### accessDB.py ###
Reads data from the database and returns the current weather data
for any city from database into a Slack message on #academy_api_testing.
Requests to input the city name that you want current weather data of  
[TM]

### sendAccessDB2Slack.py ###
Function that sends messages to Slack #academy_api_testing
[TM]

### weatherAlerts.py ###
Sends alerting message on Slack #academy_api_testing group in case of
current severe temperature conditions, comparing the current temperature
with the mean value of the predicted temperatures from the previous day.
Severe temperature conditions when temperature difference is greater than
5 degrees Celsius.
[SAH]

### provisions.txt ###
List of all the provisions needed for these scripts to work (functions
imported with their versions)

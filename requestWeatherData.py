#!/usr/bin/env python3
import json
import requests
import sys
import mysql.connector
from mysql.connector import Error

# Get woeid from DB for a specific City
mydb = mysql.connector.connect(
    host = "database.academy.grads.al-labs.co.uk",
    database = "mancWeather",
    user = "silviana",
    password = "secret"
)

def askUser(mydb):
    try:
        mycursor = mydb.cursor()
        query = " SELECT city_woeid FROM city WHERE city_name = %s """
        city_name = input("Please enter the name of the city you want weather data from: ")
        mycursor.execute(query, (city_name,))
        myresult = mycursor.fetchone()
        for city_woeid in myresult:
            print("The woeid is ", city_woeid)
    except Error as e:
        print("Error reading data from MySQL table", e)
    return int(myresult[0])

woeid = sys.argv[1]

mycursor = mydb.cursor()

def deleteWeather():
    mycursor.execute('DELETE FROM sources;')
    mycursor.execute('DELETE FROM weather_state_other_var;')
    mycursor.execute('DELETE FROM weather_state_temp;')
    mycursor.execute('DELETE FROM weather_state_wind;')
    mycursor.execute('DELETE FROM weather_forecast;')
    mycursor.execute('DELETE FROM day_parameters;')
    mycursor.execute('DELETE FROM weather_state;')

def dbweatherstate(value):
    try:
        weatherstateInsert = "INSERT INTO weather_state (weather_state_id, weather_state_name, weather_state_abbr, city_woeid) VALUES ({weather_state_id}, '{weather_state_name}', '{weather_state_abbr}', {city_woeid});"
        values ={
        'weather_state_id': value["id"],
        'weather_state_name': value["weather_state_name"],
        'weather_state_abbr': value["weather_state_abbr"],
        'city_woeid': woeid
        }
        mycursor.execute(weatherstateInsert.format(**values))
    except Error as e:
        print("Error: data already exists in the database", e)

def dbdayparameters(value):
    try:
        dayparametersInsert = "INSERT INTO day_parameters (sun_rise_time, sun_set_time, day_time,city_woeid) VALUES ('{sun_rise_time}', '{sun_set_time}', '{day_time}', {city_woeid});"
        values ={
        'sun_rise_time': value["sun_rise"],
        'sun_set_time': value["sun_set"],
        'day_time': value["time"],
        'city_woeid': woeid
        }
        mycursor.execute(weatherstateInsert.format(**values))
    except Error as e:
        print("Error: data already exists in the database", e)

def dbweatherforecast(value):
    try:
        weatherforecastInsert = "INSERT INTO weather_forecast (weather_state_id, created_date, applicable_date) VALUES ({weather_state_id}, '{created_date}', '{applicable_date}');"
        values ={
        'weather_state_id': value["id"],
        'created_date': value["created"],
        'applicable_date': value["applicable_date"],
        'city_woeid': woeid
        }
        mycursor.execute(weatherforecastInsert.format(**values))
    except Error as e:
        print("Error: data already exists in the database",e)

def dbwind(value):
    try:
        windstateInsert = "INSERT INTO weather_state_wind (weather_state_id, wind_direction_compass, wind_speed, wind_direction) VALUES ({weather_state_id}, '{wind_direction_compass}', {wind_speed}, {wind_direction});"
        values ={
        'weather_state_id': value["id"],
        'wind_direction_compass': value["wind_direction_compass"],
        'wind_speed': value["wind_speed"],
        'wind_direction': value["wind_direction"]
        }
        mycursor.execute(windstateInsert.format(**values))
    except Error as e:
        print("Error: data already exists in the database", e)

def dbtemp(value):
    try:
        tempstateInsert = "INSERT INTO weather_state_temp (weather_state_id, max_temp, min_temp, the_temp) VALUES ({weather_state_id}, {max_temp}, {min_temp}, {the_temp});"
        values ={
        'weather_state_id': value["id"],
        'max_temp': value["max_temp"],
        'min_temp': value["min_temp"],
        'the_temp': value["the_temp"]
        }
        mycursor.execute(tempstateInsert.format(**values))
    except Error as e:
        print("Error: data already exists in the database", e)

def dbothervar(value):
    try:
        weatherstateotherInsert = "INSERT INTO weather_state_other_var (weather_state_id, air_pressure, humidity, visibility, predictability) VALUES ({weather_state_id}, {air_pressure}, {humidity}, {visibility}, {predictability});"
        values ={
        'weather_state_id': value["id"],
        'air_pressure': value["air_pressure"],
        'humidity': value["humidity"],
        'visibility': value["visibility"],
        'predictability': value["predictability"]
        }
        mycursor.execute(weatherstateotherInsert.format(**values))
    except Error as e:
        print("Error: data already exists in the database", e)

def dbsources(value):
    try:
        sourcesInsert = "INSERT INTO sources (source_title, source_slug, source_url, source_crawl_rate, city_woeid) VALUES ('{source_title}', '{source_slug}', '{source_url}', {source_crawl_rate}, {city_woeid});"
        values ={
        'source_title': value["title"],
        'source_slug': value["slug"],
        'source_url': value["url"],
        'source_crawl_rate': value["crawl_rate"],
        'city_woeid': parent["woeid"]
        }
        mycursor.execute(sourcesInsert.format(**values))
    except Error as e:
        print("Error: data already exists in the database", e)

# Request data from API use get_json_page function by Tom
def get_json_page(woeid):
    url = "https://www.metaweather.com/api/location/"+str(woeid)+"/"
    r = requests.get(url)
    if r.status_code != 200:
        print("Request error")
        sys.exit(1)
    data = r.json()
    return data

def insert_data(jindata):
    #print("Successfully retrieved data for "+jindata['applicable_date'])
    if jindata['consolidated_weather']:
        for entry in jindata['consolidated_weather']:
            dbweatherstate(entry)
            dbweatherforecast(entry)
            dbwind(entry)
            dbtemp(entry)
            dbothervar(entry)
            mydb.commit()

weather4city = get_json_page(woeid)

insert_data(weather4city)

mydb.commit()
mycursor.close()
mydb.close()

-- Create mancWeather database schema

CREATE DATABASE mancWeather;

use mancWeather;

CREATE TABLE continent (
  continent_woeid INT PRIMARY KEY,
  continent_name VARCHAR(50)
);

CREATE TABLE country (
  country_woeid INT,
  country_name VARCHAR(100),
  country_latt_long VARCHAR(100),
  continent_woeid INT,
  PRIMARY KEY (country_woeid),
  FOREIGN KEY (continent_woeid) REFERENCES continent(continent_woeid)
);

CREATE TABLE province (
  province_woeid INT,
  province_name VARCHAR(100),
  province_latt_long VARCHAR(100),
  country_woeid INT,
  PRIMARY KEY (province_woeid),
  FOREIGN KEY (country_woeid) REFERENCES country(country_woeid));

CREATE TABLE city (
  city_woeid INT,
  city_name VARCHAR(100),
  city_latt_long VARCHAR(100),
  timezone_name VARCHAR(10),
  timezone_time DATETIME,
  province_woeid INT,
  country_woeid INT,
  PRIMARY KEY (city_woeid),
  FOREIGN KEY (province_woeid) REFERENCES province(province_woeid),
  FOREIGN KEY (country_woeid) REFERENCES country(country_woeid)
);

CREATE TABLE day_parameters (
  sun_rise_time DATETIME,
  sun_set_time DATETIME,
  day_time DATETIME,
  city_woeid INT,
  FOREIGN KEY (city_woeid) REFERENCES city(city_woeid)
);

CREATE TABLE weather_state (
  weather_state_id VARCHAR(100),
  weather_state_name VARCHAR(50),
  weather_state_abbr VARCHAR(10),
  city_woeid INT,
  PRIMARY KEY (weather_state_id),
  FOREIGN KEY (city_woeid) REFERENCES city(city_woeid)
);

CREATE TABLE weather_forecast (
  weather_state_id VARCHAR(100),
  created_date DATETIME,
  applicable_date DATE,
  PRIMARY KEY (weather_state_id),
  FOREIGN KEY (weather_state_id) REFERENCES weather_state(weather_state_id)
);

CREATE TABLE weather_state_wind (
  weather_state_id VARCHAR(100),
  wind_direction_compass VARCHAR(5),
  wind_speed INT,
  wind_direction INT,
  PRIMARY KEY (weather_state_id),
  FOREIGN KEY (weather_state_id) REFERENCES weather_state(weather_state_id)
);

CREATE TABLE weather_state_temp (
  weather_state_id VARCHAR(100),
  min_temp INT,
  max_temp INT,
  the_temp INT,
  PRIMARY KEY (weather_state_id),
  FOREIGN KEY (weather_state_id) REFERENCES weather_state(weather_state_id)
);

CREATE TABLE weather_state_other_var (
  weather_state_id VARCHAR(100),
  air_pressure INT,
  humidity INT,
  visibility INT,
  predictability INT,
  PRIMARY KEY (weather_state_id),
  FOREIGN KEY (weather_state_id) REFERENCES weather_state(weather_state_id)
);

CREATE TABLE sources (
  source_title VARCHAR(50),
  source_slug VARCHAR(50),
  source_url VARCHAR(100),
  source_crawl_rate INT,
  city_woeid INT,
  FOREIGN KEY (city_woeid) REFERENCES city(city_woeid)
);

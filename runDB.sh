#!/bin/bash

if (( $# != 1 ))
then
  echo "Syntax error: $0 <start/continue>"
  exit 1
fi

case $1 in
  start)
    ./createDB
    ./writeContinents2DB.py
    ./city.py

    while :; do
      while IFS= read -r line; do
        echo "Requesting info for $line"
        date >>runDB.log
        echo "$line" >>runDB.log
        ./requestWeatherData.py $line >>runDB.log
        if (( $? != 0 ))
        then
            break
        fi
        ./weatherAlerts.py $line
        if (( $? != 0 ))
        then
            break
        fi
        sleep 70
      done < city_woeid.txt
    done
  ;;
  continue)
    while :; do
      while IFS= read -r line; do
        echo "Requesting info for $line"
        date >>runDB.log
        echo "$line" >>runDB.log
        ./requestWeatherData.py $line >>runDB.log
        if (( $? != 0 ))
        then
            break
        fi
        ./weatherAlerts.py $line
        if (( $? != 0 ))
        then
            break
        fi
        sleep 70
      done < city_woeid.txt
      ./weatherAlerts.py
    done
  ;;
esac

#!/usr/bin/env python3
import json
import requests
import sys
import mysql.connector
from mysql.connector import Error
import datetime

woeid = sys.argv[1]

# Define the date and time for today
now = datetime.datetime.now()
yesterday = datetime.datetime.now() - datetime.timedelta(days = 1)
print ("Current date and time : ", now.strftime("%Y-%m-%d %H:%M:%S"))

mydb = mysql.connector.connect(
    host = "database.academy.grads.al-labs.co.uk",
    database = "mancWeather",
    user = "silviana",
    password = "secret"
)

# Determine actual temperature
try:
    mycursor = mydb.cursor()
    query =" SELECT t.weather_state_id, t.the_temp, t.min_temp, t.max_temp, wf.weather_state_id, wf.applicable_date FROM weather_state_temp t INNER JOIN weather_forecast wf ON t.weather_state_id = wf.weather_state_id WHERE wf.applicable_date = %s """
    now_date = now.strftime("%Y-%m-%d")
    mycursor.execute(query, (now_date,))
    myresult = mycursor.fetchone()
    actual_temp = myresult[1]
except:
    mydb.rollback()
    print("Error reading data from MySQL table")

print("Actual temperature is: ", actual_temp , " degrees Celsius.")

mydb.close()

# Determine predicted temperatures
def predicted_temperatures():
    URL = "https://www.metaweather.com/api/location/" + str(woeid) + "/" + now.strftime('%Y/%m/%d') + "/"
    r = requests.get(URL)
    if r.status_code != 200:
        print("Request error")
        sys.exit(1)
    weather_records = r.json()
    predicted_values = []
    for entry in weather_records:
        data = entry["created"]
        if data.startswith(yesterday.strftime("%Y-%m-%d")):
            predicted_values.append(float(entry["the_temp"]))
    mean_temp = sum(predicted_values)/len(predicted_values)
    return mean_temp

# Send alert message on Slack
def alert_msg(message):
    URL_slack = 'https://hooks.slack.com/services/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'
    obj = json.loads('{"channel": "#academy_api_testing", "username": "mancWeather", "text": "Alert! Severe weather detected: ' + message + ' "}')
    r= requests.post(URL_slack, json=obj)

# Assess the temperature conditions
def temp_alert(t, t0):
    if abs(t - t0) < 5:
        print("No need to send an alert message to the user on Slack.")
        sys.exit(1)
    elif t - t0 >= 5:
        print("Alert message sent to Slack.")
        alert_msg('high temperatures! Temperature rises to ' + str(t))
    else:
        print("Alert message sent to Slack.")
        alert_msg('low temperatures! Temperature drops to ' + str(t))

predicted_temp = predicted_temperatures()
temp_alert(actual_temp, predicted_temp)

print("Done")

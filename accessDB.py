#!/usr/bin/env python3
import mysql.connector
import datetime
import sys
import statistics as stats
import sendAccessDB2Slack as slack

mydb = mysql.connector.connect(
  host="database.academy.grads.al-labs.co.uk",
  user="thomas",
  passwd="secret",
  database="mancWeather"
)
mycursor = mydb.cursor()

def getDate():
    noDate = True
    while noDate:
        string = input("Specify a date (yyyy-mm-dd): ")
        try:
            date = datetime.datetime.strptime(string, "%Y-%m-%d").date()
            noDate = False
        except ValueError:
            print("That's not a valid date. Try again\n")
    return date

def getDBdata(cityId, date):
    getcur = mydb.cursor()
    selectStr = "SELECT TIME(wf.created_date), ws.weather_state_id, ws.weather_state_name, ws.weather_state_abbr, wst.min_temp, wst.max_temp, wst.the_temp, wsov.predictability FROM weather_state AS ws JOIN weather_forecast as wf ON ws.weather_state_id = wf.weather_state_id JOIN weather_state_temp AS wst ON ws.weather_state_id = wst.weather_state_id JOIN weather_state_other_var AS wsov ON ws.weather_state_id = wsov.weather_state_id WHERE ws.city_woeid = {id} AND DATE(wf.created_date) = '{date}' AND wf.applicable_date = '{date}' ORDER BY wf.created_date;"
    values = {'id': cityId, 'date': date}
    getcur.execute(selectStr.format(**values))
    weathers = getcur.fetchall()
    getcur.close()
    return weathers

def presentData(name, date, data):
    maxTemps = []
    minTemps = []
    theTemps = []
    preds = []
    string = "Weather data for "+str(name)+" on "+str(date)+":\n"
    for weather in data:
        string += str(weather[0])+": "+str(weather[2])+" and a temperature of "+str(weather[6])+" degrees\n"
        minTemps.append(weather[4])
        maxTemps.append(weather[5])
        theTemps.append(weather[6])
        preds.append(weather[7])
    string += "\nThe average predictability was "+str(stats.mean(preds))+"%\n"
    string += "The average max temp was "+str(stats.mean(maxTemps))+" degrees\n"
    string += "The average min temp was "+str(stats.mean(minTemps))+" degrees\n"
    string += "The highest temperature recorded was "+str(max(theTemps))+" degrees\n"
    string += "The lowest temperature recorded was "+str(min(theTemps))+" degrees\n"
    print(string)

    respSlack = input("Would you like to send this to Slack (y/n)? ")
    if respSlack == 'y' or respSlack == 'yes' or respSlack == 'Yes':
        slack.send2Slack(string)
        print("Message sent")

mycursor.execute("SELECT city.city_name, province.province_name, country.country_name, city.city_woeid FROM city JOIN country ON city.country_woeid = country.country_woeid LEFT JOIN province ON city.province_woeid = province.province_woeid;")


cities = mycursor.fetchall()
for x in cities:
    if x[1]:
        print(x[0]+', '+x[1]+', '+x[2])
    else:
        print(x[0]+', '+x[2])

gettingCity = True
while gettingCity:
    asked = input("What city would you like data for? ")
    for x in cities:
        if x[0] == asked:
            citypk = x
            gettingCity = False
    if gettingCity:
        tryagain = input("That doesn't match any city we have. Do you want to try again (y/n)? ")
        if tryagain == 'n' or tryagain == 'no' or tryagain == 'No':
            print("Okay, ending program")
            sys.exit(0)

cityName = citypk[0]
cityId = citypk[3]

date = getDate()

data = getDBdata(cityId, date)

presentData(cityName, date, data)

mydb.commit()
mycursor.close()
mydb.close()


"""
fetchall(self)
 |      Returns all rows of a query result set
 |
 |      Returns a list of tuples.
 |
 |  fetchmany(self, size=None)
 |      Returns the next set of rows of a query result, returning a
 |      list of tuples. When no more rows are available, it returns an
 |      empty list.
 |
 |      The number of rows returned can be specified using the size argument,
 |      which defaults to one
 |
 |  fetchone(self)
 |      Returns next row of a query result set
 |
 |      Returns a tuple or None.
 |
 |  getlastrowid(self)
 |      Returns the value generated for an AUTO_INCREMENT column
 """

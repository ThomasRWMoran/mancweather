#!/usr/bin/env python3
import json
import requests
import sys
import mysql.connector


mydb = mysql.connector.connect(
  host="database.academy.grads.al-labs.co.uk",
  user="thomas",
  passwd="secret",
  database="mancWeather"
)

mycursor = mydb.cursor()

mycursor.execute('DELETE FROM city;')
mycursor.execute('DELETE FROM province;')
mycursor.execute('DELETE FROM country;')

needCountryID4Province = {}

def dbCountry(country, parent):
    countryInsert = "INSERT INTO country (country_woeid, country_name, country_latt_long, continent_woeid) VALUES ({country_woeid}, '{country_name}', '{country_latt_long}', {continent_woeid});"
    values = {
    'country_woeid': country['woeid'],
    'country_name': country['title'],
    'country_latt_long': country['latt_long'],
    'continent_woeid': parent['woeid'],
    }
    #print(countryInsert.format(**values))
    mycursor.execute(countryInsert.format(**values))
def dbProvince(province, parent):
    provinceInsert = "INSERT INTO province (province_woeid, province_name, province_latt_long, country_woeid) VALUES ({province_woeid}, '{province_name}', '{province_latt_long}', {country_woeid});"
    values = {
    'province_woeid': province['woeid'],
    'province_name': province['title'],
    'province_latt_long': province['latt_long'],
    'country_woeid': parent['woeid'],
    }
    #print(provinceInsert.format(**values))
    mycursor.execute(provinceInsert.format(**values))
def dbCity(city, parent):
    if parent['location_type'] == 'Country':
        cityInsert = "INSERT INTO city (city_woeid, city_name, city_latt_long, country_woeid) VALUES ({city_woeid}, '{city_name}', '{city_latt_long}', {country_woeid});"
        values = {
        'city_woeid': city['woeid'],
        'city_name': city['title'],
        'city_latt_long': city['latt_long'],
        'country_woeid': parent['woeid'],
        }
        #print(cityInsert.format(**values))
        mycursor.execute(cityInsert.format(**values))
    elif parent['location_type'] == 'Region / State / Province':
        cityInsert = "INSERT INTO city (city_woeid, city_name, city_latt_long, province_woeid, country_woeid) VALUES ({city_woeid}, '{city_name}', '{city_latt_long}', {province_woeid}, {country_woeid});"
        values = {
        'city_woeid': city['woeid'],
        'city_name': city['title'],
        'city_latt_long': city['latt_long'],
        'province_woeid': parent['woeid'],
        'country_woeid': needCountryID4Province[str(parent['woeid'])],
        }
        #print(cityInsert.format(**values))
        mycursor.execute(cityInsert.format(**values))
    else:
        print(city['names']+' has a weird parent, so I am not adding it to the database')

def get_json_page(woeid):
    url = "https://www.metaweather.com/api/location/"+str(woeid)+"/"
    r = requests.get(url)
    if r.status_code != 200:
        print("Request error")
        sys.exit(1)
    data = r.json()
    return data

def add_children(jindata):
    global needCountryID4Province
    print("Successfully retrieved data for "+jindata['title'])
    non_city = []
    if jindata['children']:
        for child in jindata['children']:
            if child['location_type'] == 'City':
                child.update({'parent': jindata['title']})
                city_file.write(str(child['woeid'])+'\n')
                dbCity(child,jindata)
            else:
                non_city.append(child['woeid'])
                if child['location_type'] == 'Country':
                    dbCountry(child, jindata)
                elif child['location_type'] == 'Region / State / Province':
                    dbProvince(child, jindata)
                    needCountryID4Province[str(child['woeid'])] = str(jindata['woeid'])
                else:
                    print('I have no godly clue what '+child['name']+' is, it will not be in database')
    else:
        print(jindata['title']+" has no children (this should not happen)")
    return non_city

def try_woeid(woeid):
    while len(woeid) > 0:
        childwoeid = []
        for wotry in woeid:
            childwoeid += add_children(get_json_page(wotry))
        woeid = childwoeid

city_file = open("city_woeid.txt", "w")

mycursor.execute("SELECT continent_woeid FROM continent;")
continentIds = mycursor.fetchall()

for id in continentIds:
    try_woeid(id)

city_file.close()

mydb.commit()
mycursor.close()
mydb.close()

#!/usr/bin/env python3
import sys
import mysql.connector

mydb = mysql.connector.connect(
  host="database.academy.grads.al-labs.co.uk",
  user="thomas",
  passwd="secret",
  database="mancWeather"
)
mycursor = mydb.cursor()

mycursor.execute('DELETE FROM continent;')

continentInsert = "INSERT INTO continent (continent_woeid, continent_name) VALUES ({woeid},'{name}');"

continents = [{'woeid': '24865675', 'name': 'Europe'}]

for continent in continents:
    print('Uploading '+continent['name']+' to database\n')
    line = continentInsert.format(**continent)
    mycursor.execute(line)

mydb.commit()
mycursor.close()
mydb.close()
